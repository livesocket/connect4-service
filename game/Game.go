package game

import (
	"errors"

	"gitlab.com/livesocket/conv"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/connect4-service/models"
	"gitlab.com/livesocket/service"
)

var activeConnect4 *models.Connect4
var turn string
var players map[string]uint

func GetActive() *models.Connect4 {
	return activeConnect4
}

func Start(c4 *models.Connect4) error {
	_, err := service.Socket.SimpleCall("connect4.start", wamp.List{c4.Player1, c4.Player2}, nil)
	if err != nil {
		return err
	}

	if activeConnect4 != nil {
		return errors.New("another game already in progress, please wait for it to finish before accepting")
	}

	activeConnect4 = c4
	players = map[string]uint{
		c4.Player1: 1,
		c4.Player2: 2,
	}
	return nil
}

func Move(player string, column uint) error {
	// Check if game exists
	if activeConnect4 == nil {
		return errors.New("no game in progress")
	}

	// Check if player in game
	if activeConnect4.Player1 != player && activeConnect4.Player2 != player {
		// Ignore
		return nil
	}

	// Check if is players turn
	if turn != player {
		// Ignore
		return nil
	}

	// Check if valid column
	if column < 1 || column > 7 {
		return errors.New("@" + player + " invalid column. Choose 1-7.")
	}

	// Tell game to do move
	result, err := service.Socket.SimpleCall("connect4.move", wamp.List{players[player], column}, nil)
	if err != nil {
		return err
	}

	// Check if game has been won
	if len(result.Arguments) > 0 {
		// Lookup player name
		for key, value := range players {
			// If player matches
			if pName, err := conv.ToUint(result.Arguments[0]); err != nil {
				// Player wins
				if value == pName {
					return win(key)
				}
				continue
			} else {
				return errors.New("player not found in game")
			}
		}

		return errors.New("could not locate winner")
	}

	return nil
}

func win(winner string) error {
	activeConnect4.Winner = winner
	activeConnect4.Active = false
	_, err := activeConnect4.Update()
	return err
}
