package models

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"gitlab.com/livesocket/service"
)

type Connect4 struct {
	ID        uint       `db:"id" json:"id"`
	Channel   string     `db:"channel" json:"channel"`
	Player1   string     `db:"player1" json:"player1"`
	Player2   string     `db:"player2" json:"player2"`
	Active    bool       `db:"active" json:"active"`
	Winner    string     `db:"winner" json:"winner"`
	UpdatedAt *time.Time `db:"updated_at" json:"updated_at"`
}

func NewConnect4(channel, player1, player2 string) (*Connect4, error) {
	now := time.Now()

	connect4 := &Connect4{
		ID:        0,
		Player1:   player1,
		Player2:   player2,
		Channel:   channel,
		Active:    true,
		Winner:    "",
		UpdatedAt: &now,
	}
	return connect4.Fetch()
}

func FindById(id uint) (*Connect4, error) {
	connect4 := Connect4{}
	err := service.DB.Get(&connect4, "SELECT * FROM `connect4` WHERE `id`=?", id)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			return nil, nil
		}
		return nil, err
	}
	return &connect4, nil
}

func FindActiveInChannel(channel, acceptor string) (*Connect4, error) {
	connect4 := Connect4{}
	err := service.DB.Get(&connect4, "SELECT * FROM `connect4` WHERE `channel`=? AND `player2`=? AND `active`=?", channel, acceptor, true)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			return nil, nil
		}
		return nil, err
	}
	return &connect4, nil
}

func (c *Connect4) Fetch() (*Connect4, error) {
	connect4 := Connect4{}
	err := service.DB.Get(&connect4, "SELECT * FROM `connect4` WHERE `player1`=? AND `player2`=? AND `channel`=? AND `updated_at`=?", c.Player1, c.Player2, c.Channel, c.UpdatedAt)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			return nil, nil
		}
		return nil, err
	}
	return &connect4, nil
}

func (c *Connect4) Create() (sql.Result, error) {
	return service.DB.NamedExec("INSERT INTO `connect4` (`channel`,`player1`,`player2`,`active`,`winner`,`updated_at`) VALUES (:channel,:player1,:player2,:active,:winner,:updated_at)", c)
}

func (c *Connect4) Update() (sql.Result, error) {
	return service.DB.NamedExec("UPDATE `connect4` SET `active`=:active,`winner`=:winner,`updated_at`=CURRENT_TIMESTAMP WHERE `id`=:id", c)
}
