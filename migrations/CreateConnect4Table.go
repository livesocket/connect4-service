package migrations

import (
	"github.com/jmoiron/sqlx"
)

func CreateConnect4Table(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `connect4` (`id` INT UNSIGNED AUTO_INCREMENT,`channel` varchar(255) NOT NULL,`player1` varchar(255) NOT NULL,`player2` varchar(255) NOT NULL,`active` BOOL DEFAULT 1 NOT NULL,`winner` varchar(255) NULL,`updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,PRIMARY KEY(`id`))")
	return err
}
