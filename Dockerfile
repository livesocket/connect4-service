FROM golang:1.12-alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/connect4-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/connect4-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o connect4-service

FROM scratch as release
COPY --from=builder /repos/connect4-service/connect4-service /connect4-service
EXPOSE 8080
ENTRYPOINT ["/connect4-service"]
