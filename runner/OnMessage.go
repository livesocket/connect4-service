package runner

import (
	"fmt"
	"log"
	"regexp"

	"gitlab.com/livesocket/conv"

	"gitlab.com/livesocket/connect4-service/game"

	"github.com/gammazero/nexus/v3/wamp"
	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

// OnMessageSubscription Handler for detection and handling connect4 game control chat messages arive
var OnMessageSubscription = lib.Subscription{
	Topic:   "event.chat.message",
	Handler: onMessage,
}

func onMessage(event *wamp.Event) {
	if game.GetActive() == nil {
		return
	}

	// If message exists
	if len(event.Arguments) > 0 {
		// Convert json message to twitch.PrivateMessage
		message := service.MapToPrivateMessage(event.Arguments[0].(map[string]interface{}))
		println(message.Message)

		// Check is message contains a command
		regex := regexp.MustCompile(`^\d\s*`)
		if regex.Match([]byte(message.Message)) {
			log.Print("Connect4 game move detected")

			//
			// Run command
			player, column, err := parseMessage(message)
			if err != nil {
				fmt.Printf("%v", err)
			}
			go game.Move(player, column)
		}
	}
}

func parseMessage(message *twitch.PrivateMessage) (player string, column uint, err error) {
	player = message.User.Name
	column, err = conv.ToUint(message.Message[0])
	return
}
