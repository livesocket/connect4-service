package commands

import (
	"errors"

	"gitlab.com/livesocket/connect4-service/game"

	"gitlab.com/livesocket/connect4-service/models"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/conv"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

var Connect4Command = lib.Action{
	Proc:    "command.connect4",
	Handler: connect4,
}

type connect4Input struct {
	Channel   string
	Requestor string
	Requestee string
}

func connect4(invocation *wamp.Invocation) client.InvokeResult {
	// Get connect4 input options
	input, err := getConnect4Input(invocation)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Check if challenging command
	if input.Requestee != "" {
		return connect4Challenge(input)
	}

	// Check if requestee of active challenge
	c4, err := models.FindActiveInChannel(input.Channel, input.Requestor)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// If active challenge exists
	if c4 != nil {
		return connect4Accept(c4)
	}

	// Otherwise display help info
	return lib.ArgsResult("You must challenge someone to a game. Try: !connect4 <username>")
}

func connect4Challenge(input *connect4Input) client.InvokeResult {
	// Create new game of Connect4 in the DB
	c4, err := models.NewConnect4(input.Channel, input.Requestor, input.Requestee)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Ask challenged player to accept the game request
	return lib.ArgsResult("@" + c4.Player2 + " you have been challenged to a game of Connect4 by " + c4.Player1 + ". To accept use command '!connect4'")
}

func connect4Accept(c4 *models.Connect4) client.InvokeResult {
	err := game.Start(c4)
	if err != nil {
		return lib.ErrorResult(err)
	}

	return lib.ArgsResult("Connect4 game started! @" + c4.Player1 + " you are Red, @" + c4.Player2 + " you are Blue. To play enter the column number in chat where you would like your piece placed.")
}

func getConnect4Input(invocation *wamp.Invocation) (*connect4Input, error) {

	if invocation.ArgumentsKw["twitch"] == nil {
		return nil, errors.New("missing twitch message for custom command")
	}

	twitch := service.MapToPrivateMessage(invocation.ArgumentsKw["twitch"].(map[string]interface{}))

	requestee := ""
	if len(invocation.Arguments) > 0 {
		requestee = conv.ToString(invocation.Arguments[0])
	}

	return &connect4Input{
		Channel:   twitch.Channel,
		Requestor: twitch.User.Name,
		Requestee: requestee,
	}, nil
}
