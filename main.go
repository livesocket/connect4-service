package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/connect4-service/migrations"
	"gitlab.com/livesocket/connect4-service/runner"

	"gitlab.com/livesocket/connect4-service/commands"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	close, err := service.NewStandardService([]lib.Action{
		commands.Connect4Command,
	}, []lib.Subscription{runner.OnMessageSubscription}, "__connect4_service", migrations.CreateConnect4Table)
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Done():
		log.Print("Router gone, exiting")
		return
	}
}
