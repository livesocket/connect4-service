module gitlab.com/livesocket/connect4-service

go 1.12

require (
	github.com/gammazero/nexus/v3 v3.0.0
	github.com/gempir/go-twitch-irc/v2 v2.2.1
	github.com/jmoiron/sqlx v1.2.0
	gitlab.com/livesocket/conv v0.0.0-20191012101209-727e1c03a95c
	gitlab.com/livesocket/service v1.4.3
)
